# LAKOTA LINGUISTIC TEXT CORPORA

(From <https://aisri.indiana.edu/research/lakotacorpora.html>)

Ella C. Deloria (1889-1971), a member of the Yankton Sioux tribe, was brought
up in South Dakota on the Standing Rock Reservation. A native speaker of
Lakota, she received her Bachelor of Science degree from Columbia Teachers'
College in 1915. While in New York she met the anthropologist Franz Boas and
served as a consultant on the Lakota language in Boas's class on field
linguistics. After graduating she worked in the field of American Indian
education. In 1927 she began linguistic and ethnological work on Lakota under
Boas's supervision. From 1928 through 1938 Deloria devoted most of her time to
documenting Lakota language and culture. One outcome of that work was Dakota
Grammar (Boas and Deloria 1941), now a classic in American Indian linguistics.
Another important product was a collection of some 117 Lakota language texts
that Deloria wrote down, with word-for-word and free translations. She
published 63 of them (Dakota Texts, 1932); the remainder, more than half of the
collection, remain unpublished and are archived with the Boas Collection in the
Library of the American Philosophical Society (APS), Philadelphia.

From 1999 to 2002 the National Endowment for the Humanities funded the "Lakota
Texts" project with a grant to Raymond J. DeMallie and colleagues at the
American Indian Studies Research Institute, Indiana University (Collaborative
Research grant number RZ-20438-99). The project sought to transcribe,
translate, and annotate historical Lakota texts. Dr. Douglas R. Parks provided
overall linguistic direction to the project. Dr. Paul Kroeber worked on
linguistic annotation. Dr. Wallace Hooper provided the software applications
and oversaw computer work.

The unpublished texts of Ella Deloria were the major focus of the grant. The
entire collection was transcribed in ATP (Annotated Text Processor), a utility
designed by Dr. Hooper. Dr. Kroeber provided word and morpheme glosses. This
body of texts makes available to scholars at large a vast store of linguistic,
historical, and cultural information. A representative sample of these texts,
including each of the major textual genres, is presented on this website. They
amount to approximately 350 pages, in .pdf format.

## List of Texts

* Geo. Schmidt’s Vision Experience
* The Buffalo People
* Walega-Hoksila
* An old-time custom, at meals
* A typical "kidding" between brothers-in-law
* Origin of the name "Oglala"
* Speech at a Thanksgiving Feast

## Notes & Questions

* These interesting gloss files are very hard to process programatically. The
  PDFs are not formatted in a way that allows me to copy and paste from them.
* What orthography is this?




