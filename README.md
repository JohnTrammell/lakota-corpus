# Lakota Corpus

Lakota language corpus, gloss parser, NLP




## Orthographies

There are way too many Lakota orthographies, including:

* Sioiuanist
* Riggs 1852 
* Boas & Swanton, Deloria 1910/1932
* Buechel 1939
* Manhart 1970
* Traditional
* Colorado University 1970
* Ullrich 1992
* White Hat 1973
* Saskatchewan Indigenous Cultural Centre (SICC)
* Txakini
* Net Siouan

## Resources

* https://www.languagegeek.com/siouan/lakota.html
* https://lakhotiyapi.wordpress.com/
* https://blog.library.si.edu/blog/2013/05/10/historical-sioux-tribal-newspapers-see-the-light-of-day/
* https://sittingbull.edu/sitting-bull-college/students/library/native-american-studies-resources/
* https://babel.hathitrust.org/cgi/pt?id=uiuo.ark:/13960/t8sb43q36&view=1up&seq=7
* https://archive.org/search.php?query=subject%3A%22Dakota+Glossed+Text%22


